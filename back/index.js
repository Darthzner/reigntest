import express, { json, urlencoded, static } from 'express';
import mongose from 'mongoose';
import cors from 'cors';
const port = 3001


import { join } from 'path';


const app = express();

dotenv.config({
    path: './config.env'
});


app.use(cors());
app.use(logger('dev'));
app.use(json());
app.use(urlencoded({ extended: false }));
app.use(require('./routes/Routes'));
app.use(static(join(__dirname, '/files')))



app.use('*', (req, res, next) => {
    const err = new AppError(404, 'fail', 'undefined route');
    next(err, req, res, next);
});

app.listen(port, ()=> {
    
    console.log(`Server running at http://localhost:${port}`);
    
    
});

export default app;